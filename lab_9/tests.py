from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .api_enterkomputer import get_drones, get_soundcards, get_opticals
from .views import index, response
import environ
import requests

root = environ.Path(__file__) - 3
env = environ.Env(DEBUG=(bool, False), )
environ.Env.read_env('file.env')

# Create your tests here.
class Lab9UnitTest(TestCase):

    def test_lab_9_url_is_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code, 200)

    def test_lab9_using_index_func(self):
        found = resolve('/lab-9/')
        self.assertEqual(found.func, index)

    def test_root_url_now_is_using_index_page_from_lab_9(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)

    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")

    def test_lab_9_index_condition(self):
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 200)

        # logged in, redirect to profile page
        self.assertNotIn("user_login", self.client.session)
        html_content = response.content.decode('utf-8')
        self.assertIn("Halaman Login", html_content)

        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)

        login = self.client.get('/lab-9/')
        self.assertEqual(login.status_code, 302)

    def test_profile(self):
        # Not logged in
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 302)

        # Logged in
        self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 200)

	# test api enterkomputer
    def test_get_drones(self):
        response = get_drones()
        self.assertEqual('<Response [200]>', str(response))
        self.assertEqual(type(response), requests.Response)

    def test_get_optical(self):
        response = get_opticals()
        self.assertEqual('<Response [200]>', str(response))
        self.assertEqual(type(response), requests.Response)

    def test_get_soundcard(self):
        response = get_soundcards()
        self.assertEqual('<Response [200]>', str(response))
        self.assertEqual(type(response), requests.Response)

